#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.model.repository.person;


import ${package}.model.entities.Person;
import ${package}.model.repository.base.BaseRepository;

public interface PersonRepository extends BaseRepository<Person, Long> {

}
