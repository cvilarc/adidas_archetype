#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import ${package}.model.entities.Person;
import ${package}.model.repository.person.PersonRepository;
import ${package}.service.PersonService;

public class PersonServiceImpl implements PersonService {
	
	private final static String PERSON_CACHE_NAME = "personCache";

	@Autowired(required=true)
	private PersonRepository personRepository; 
	
	@CacheEvict(value=PERSON_CACHE_NAME, allEntries=true)
	public Long createPerson(String name, String surname) {
		Person person = new Person(name, surname);
		
		person = personRepository.save(person);

		return person.getId();
	}

	@Cacheable(value=PERSON_CACHE_NAME)
	public List<Person> list() {
		return personRepository.findAll();
	}

}
