#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ${package}.model.entities.Person;

@Service
@Transactional(readOnly=true)
public interface PersonService {
	
	public Long createPerson(String name, String surname);

	public List<Person> list();

}
