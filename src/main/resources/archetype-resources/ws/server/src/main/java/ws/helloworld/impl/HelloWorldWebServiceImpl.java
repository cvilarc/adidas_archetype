#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${parentArtifactId}.helloworld.impl;

import javax.j${parentArtifactId}.WebService;

import ${package}.${parentArtifactId}.helloworld.HelloWorldWebService;

@WebService(endpointInterface="${package}.${parentArtifactId}.helloworld.HelloWorldWebService")
public class HelloWorldWebServiceImpl implements HelloWorldWebService {

	@Override
	public String sayHello() {
		return "Hello";
	}

	@Override
	public String sayHi(String greeting) {
		return "Hi dear " + greeting;
	}

}
