#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${parentArtifactId}.person.impl;

import javax.j${parentArtifactId}.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import ${package}.${parentArtifactId}.person.PersonWebService;
import ${package}.service.PersonService;

@Service
@WebService(endpointInterface = "${package}.${parentArtifactId}.person.PersonWebService")
public class PersonWebServiceImpl extends SpringBeanAutowiringSupport implements PersonWebService {

	@Autowired
	private PersonService personService;
	
	@Override
	public String createPerson(String name, String surname) {
		return personService.createPerson(name, surname).toString();
	}
	
}
