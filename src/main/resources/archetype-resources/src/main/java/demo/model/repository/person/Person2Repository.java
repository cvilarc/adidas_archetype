#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.demo.model.repository.person;

import org.springframework.data.repository.CrudRepository;

import ${package}.demo.model.entities.Person;

public interface Person2Repository extends CrudRepository<Person, Long> {

}
