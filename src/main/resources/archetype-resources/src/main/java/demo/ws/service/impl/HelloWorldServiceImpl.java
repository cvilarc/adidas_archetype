#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.demo.ws.service.impl;

import javax.jws.WebService;

import ${package}.demo.ws.service.HelloWorldService;

@WebService(endpointInterface = "${package}.demo.ws.service.HelloWorldService")
public class HelloWorldServiceImpl implements HelloWorldService {

	@Override
	public String sayHello() {
		return "Hello";
	}
	
	@Override
	public String sayHi(String greeting) {
        System.out.println("sayHi called");
        return "Hello " + greeting;
    }

}
