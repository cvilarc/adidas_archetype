#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.demo.view;

import java.io.IOException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import ${package}.demo.model.entities.Person;
import ${package}.demo.service.PersonService;
import ${package}.demo.ws.person.PersonWebService;

public class DistanceHttpRequestHandler extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2253591118169152340L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("doGet");
		try {
			InitialContext ctx = new InitialContext();
			Object aux = ctx.lookup("java:comp/env/jdbc/demoDS");
			System.out.println("check it");
			WebApplicationContext wac = WebApplicationContextUtils.getWebApplicationContext(req.getServletContext());
			PersonService personService = wac.getBean(PersonService.class);
			personService.createPerson("Carlos", "Vilar");
			for (Person person : personService.list()) {
				System.out.println(person);
			}
			
			//PersonWebService personClient = (PersonWebService) wac.getBean("personClient");
			
			PersonWebService otro = (PersonWebService) wac.getBean("personClient");
			
			String out = otro.createPerson("Laura", "Castro");
			System.out.println(out);
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		forward(req, resp);
	}

	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException,
			IOException {
		System.out.println("doPost");
		String srcCity = request.getParameter("srcCity");
		String destCity = request.getParameter("destCity");
		double distance = 20.0d;
		request.setAttribute("distance", distance);
		System.out.println("distance = " + distance);
		forward(request, response);
	}

	private void forward(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/pages/distance.jsp");
		dispatcher.forward(request, response);
	}
}
