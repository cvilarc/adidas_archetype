#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.demo.client;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ${package}.demo.model.entities.Person;
import ${package}.demo.model.repository.person.PersonRepository;
import ${package}.demo.service.PersonService;
import ${package}.demo.ws.helloworld.HelloWorldWebService;
import ${package}.demo.ws.person.PersonWebService;

public class ClientApp {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring-ws-client.xml");

		doMain3(context);

		context.close();
	}

	public static void doMain1(ApplicationContext context) {
		Person peter = new Person("Peter", "Sagan");
		Person nasta = new Person("Nasta", "Kuzminova");

		peter.setId(null);
		nasta.setId(null);
		PersonRepository personRepository = (PersonRepository) context.getBean(PersonRepository.class);
		personRepository.save(peter);
		personRepository.save(nasta);

		List<Person> persons = personRepository.findAll();
		for (Person person : persons) {
			System.out.println(person);
		}
	}

	public static void doMain2(ApplicationContext context) {
		PersonService personService = context.getBean(PersonService.class);

		personService.createPerson("Peter", "Sagan");
		personService.createPerson("Nasta", "Kuzminova");

		List<Person> personList = personService.list();
		for (Person person : personList) {
			System.out.println(person);
		}
	}

	public static void doMain3(ApplicationContext context) {
		PersonWebService personClient = context.getBean(PersonWebService.class);
		HelloWorldWebService helloClient = context.getBean(HelloWorldWebService.class);

		System.out.println(helloClient.sayHello());
		System.out.println(helloClient.sayHi("Carlos"));

		personClient.createPerson("Carlos", "Vilar");

//		PersonService personService = context.getBean(PersonService.class);
//
//		personService.createPerson("Peter", "Sagan");
//		personService.createPerson("Nasta", "Kuzminova");
//
//		List<Person> personList = personService.list();
//		for (Person person : personList) {
//			System.out.println(person);
//		}

	}
}
