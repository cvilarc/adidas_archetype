#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.demo.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ${package}.demo.model.entities.Person;
import ${package}.demo.model.repository.person.PersonRepository;
import ${package}.demo.service.PersonService;

public class PersonServiceImpl implements PersonService {

	@Autowired(required=true)
	private PersonRepository personRepository; 
	
	@Override
	public Long createPerson(String name, String surname) {
		Person person = new Person(name, surname);
		
		person = personRepository.save(person);

		return person.getId();
	}

	@Override
	public List<Person> list() {
		List<Person> out = new ArrayList<Person>();
		for (Person person : personRepository.findAll()) {
			out.add(person);
		}
		return out;
	}

}
