#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.demo.ws.person.impl;

import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import ${package}.demo.service.PersonService;
import ${package}.demo.ws.person.PersonWebService;

@Service
@WebService(endpointInterface = "${package}.demo.ws.person.PersonWebService")
public class PersonWebServiceImpl extends SpringBeanAutowiringSupport implements PersonWebService {

	@Autowired
	private PersonService personService;
	
	@Override
	public String createPerson(String name, String surname) {
		return personService.createPerson(name, surname).toString();
	}
	
}
