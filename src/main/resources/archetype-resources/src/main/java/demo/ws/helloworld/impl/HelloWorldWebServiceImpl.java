#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.demo.ws.helloworld.impl;

import javax.jws.WebService;

import ${package}.demo.ws.helloworld.HelloWorldWebService;

@WebService(endpointInterface="${package}.demo.ws.helloworld.HelloWorldWebService")
public class HelloWorldWebServiceImpl implements HelloWorldWebService {

	@Override
	public String sayHello() {
		return "Hello";
	}

	@Override
	public String sayHi(String greeting) {
		return "Hi dear " + greeting;
	}

}
