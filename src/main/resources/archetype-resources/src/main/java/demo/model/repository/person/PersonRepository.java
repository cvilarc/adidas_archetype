#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.demo.model.repository.person;


import ${package}.demo.model.entities.Person;
import ${package}.demo.model.repository.base.BaseRepository;

public interface PersonRepository extends BaseRepository<Person, Long> {

}
