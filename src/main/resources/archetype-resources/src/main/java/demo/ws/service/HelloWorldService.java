#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.demo.ws.service;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface HelloWorldService {
	
	public String sayHello();
	
    public String sayHi(@WebParam(name="greeting") String greeting);
}